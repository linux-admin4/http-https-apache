Чтобы настроить перенаправление HTTP на HTTPS на сервере Apache, выполните следующие шаги:

1. **Установите SSL-модуль для Apache**:
   
   Убедитесь, что SSL-модуль установлен и включен. На Ubuntu или Debian это можно сделать с помощью:

   ```bash
   sudo a2enmod ssl
   sudo service apache2 restart
   ```

2. **Получите SSL-сертификат**:
   
   Вы можете получить SSL-сертификат от центра сертификации (например, Let's Encrypt). Для Let's Encrypt используйте certbot:

   ```bash
   sudo apt-get install certbot python3-certbot-apache
   sudo certbot --apache
   ```

   Следуйте инструкциям на экране, чтобы установить сертификат.

3. **Настройте виртуальный хост для HTTPS**:
   
   Откройте файл конфигурации вашего виртуального хоста для редактирования. Обычно это файл в `/etc/apache2/sites-available/` с именем вашего сайта, например, `000-default.conf`. Убедитесь, что у вас есть следующий блок для HTTPS:

   ```apache
   <VirtualHost *:443>
       ServerName www.example.com
       DocumentRoot /var/www/html

       SSLEngine on
       SSLCertificateFile /etc/letsencrypt/live/example.com/fullchain.pem
       SSLCertificateKeyFile /etc/letsencrypt/live/example.com/privkey.pem
       Include /etc/letsencrypt/options-ssl-apache.conf

       # Дополнительные настройки
   </VirtualHost>
   ```

   Обновите путь к сертификатам и ключам в соответствии с вашими файлами.

4. **Настройте перенаправление с HTTP на HTTPS**:
   
   В файл конфигурации вашего виртуального хоста для HTTP (обычно `000-default.conf`), добавьте следующий блок, чтобы перенаправлять весь трафик с HTTP на HTTPS:

   ```apache
   <VirtualHost *:80>
       ServerName www.example.com
       DocumentRoot /var/www/html

       RewriteEngine On
       RewriteCond %{HTTPS} off
       RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R=301,L]

       # Дополнительные настройки
   </VirtualHost>
   ```

5. **Перезагрузите Apache**:

   После внесения изменений перезагрузите Apache, чтобы они вступили в силу:

   ```bash
   sudo service apache2 restart
   ```

После выполнения этих шагов все запросы на HTTP будут перенаправляться на HTTPS. Убедитесь, что ваши файлы конфигурации Apache корректны и не содержат синтаксических ошибок, иначе сервер не сможет перезапуститься.